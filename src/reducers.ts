import type { State } from './types';
import { userEndpoint } from './util';
import { createStore, applyMiddleware, AnyAction } from 'redux';
import thunk, { ThunkDispatch } from 'redux-thunk';

const initialState: State = {
  userList: [],
};

function rootReducer(
  state = initialState,
  action: { type: string; payload: any }
): State {
  switch (action.type) {
    case 'SET_USER_LIST':
      return { ...state, userList: action.payload };
    default:
      return state;
  }
}

const store = createStore(rootReducer, applyMiddleware(thunk));
export default store;

export const fetchUserList =
  () => (dispatch: ThunkDispatch<State, void, AnyAction>) => {
    fetch(userEndpoint)
      .then((v) => v.json())
      .then((v) => dispatch({ type: 'SET_USER_LIST', payload: v }));
  };
