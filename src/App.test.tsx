import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import App from './App';
import { deleteUser } from './util';
jest.mock('./util', () => {
  return {
    ...jest.requireActual('./util'),
    getUserList: () =>
      Promise.resolve([
        {
          id: 1,
          first_name: 'Sherlock',
          last_name: 'Holmes',
          gender: 'male',
          age: 39,
          address: '221B Baker Street',
        },
      ]),
    deleteUser: jest.fn(),
  };
});

describe('User list test', () => {
  test('should render user list', async () => {
    render(<App />);
    await waitFor(() => {
      ['Id', 'Action', 1, 'Sherlock', 'Holmes', 'male', 39].forEach((v) => {
        const text = screen.getByText(v);
        expect(text).toBeInTheDocument();
      });
    });
    const deleteBtn = screen.getAllByText('Delete');
    expect(deleteBtn.length).toBe(1);
  });
  test('should delete user 1', async () => {
    (deleteUser as any).mockImplementation(() => Promise.resolve());
    render(<App />);
    let deleteBtn: any;
    await waitFor(() => {
      deleteBtn = screen.getByText('Delete');
    });
    fireEvent.click(deleteBtn);
    expect(deleteUser).toBeCalledWith(1);
    expect(deleteUser).toHaveBeenCalled();
  });
});
