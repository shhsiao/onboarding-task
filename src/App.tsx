import { useEffect, useState } from 'react';
import { transTableTitle, addUser, deleteUser } from './util';
import type { User, State } from './types';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import UserForm from './components/UserForm';
import './App.css';
import { Button } from '@mui/material';
import { useSelector, useDispatch } from 'react-redux';
import { fetchUserList } from './reducers';

const initUser: User = {
  first_name: '',
  last_name: '',
  age: 0,
  gender: 'male',
  address: '',
};
function App() {
  const userList = useSelector((state: State) => state.userList);
  const dispatch = useDispatch();
  const [tempUser, setTempUser] = useState<User>(initUser);
  const handleUserInput = (key: string, value: string) => {
    const v: string | number = key === 'age' ? +value : value;
    setTempUser({ ...tempUser, [key]: v });
  };
  const getUserList = () => {
    dispatch(fetchUserList());
  };
  const handleAddUserClick = () => {
    addUser(tempUser)
      .then(() => {
        getUserList();
      })
      .finally(() => setTempUser(initUser));
  };
  const handleDeleteUserClick = (id: number) => {
    deleteUser(id).then(() => {
      getUserList();
    });
  };
  useEffect(() => {
    getUserList();
  }, []);

  return (
    <>
      <UserForm
        handleUserInput={handleUserInput}
        tempUser={tempUser}
        addUser={handleAddUserClick}
      />
      {userList.length ? (
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow>
                {[...Object.keys(userList[0]), 'Action']
                  .map((v) => transTableTitle(v))
                  .map((v) => (
                    <TableCell key={v} sx={{ fontWeight: 'bold' }}>
                      {v}
                    </TableCell>
                  ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {userList.map((user: any) => (
                <TableRow key={user.id}>
                  {Object.keys(userList[0]).map((v) => (
                    <TableCell
                      key={v}
                      sx={{ fontWeight: v === 'id' ? 'bold' : 'normal' }}
                    >
                      {user[v]}
                    </TableCell>
                  ))}
                  <TableCell>
                    <Button
                      variant="contained"
                      onClick={() => handleDeleteUserClick(user.id)}
                      color="error"
                    >
                      Delete
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      ) : null}
    </>
  );
}

export default App;
