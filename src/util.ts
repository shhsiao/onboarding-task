import type { User } from './types';

export const userEndpoint = 'http://localhost:8000/users';

export const transTableTitle = (s: string) => {
  s = s.replace('_', ' ');
  return s[0].toUpperCase() + s.slice(1);
};

export const getUserList = () => {
  return fetch(userEndpoint).then((res) => res.json());
};

export const addUser = (user: User) => {
  return fetch(userEndpoint, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(user),
  }).then((res) => res.json());
};

export const deleteUser = (id: number) => {
  return fetch(`${userEndpoint}/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
  });
};
