import TextField from '@mui/material/TextField';
import styles from './styles.module.css';
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import { Button } from '@mui/material';
import { User } from '../../types';

const UserForm: React.FC<{
  handleUserInput: Function;
  tempUser: User;
  addUser: Function;
}> = ({ handleUserInput, tempUser, addUser }) => {
  return (
    <>
      <div className={styles.container}>
        <h2>Users</h2>
        <div className={styles.input}>
          <label htmlFor="first_name">First Name</label>
          <TextField
            fullWidth
            value={tempUser.first_name}
            placeholder="Enter First Name"
            id="first_name"
            onChange={(e) => handleUserInput(e.target.id, e.target.value)}
          />
        </div>
        <div className={styles.input}>
          <label htmlFor="last_name">Last Name</label>
          <TextField
            fullWidth
            required
            value={tempUser.last_name}
            placeholder="Enter Last Name"
            id="last_name"
            onChange={(e) => handleUserInput(e.target.id, e.target.value)}
          />
        </div>
        <div className={styles.input}>
          <label htmlFor='age'>Age</label>
          <TextField
            fullWidth
            required
            value={tempUser.age}
            id="age"
            type="number"
            inputProps={{ min: 0, max: 100 }}
            onChange={(e) => handleUserInput(e.target.id, e.target.value)}
          />
        </div>
        <div className={styles.input}>
          <label htmlFor='gender'>Gender</label>
          <RadioGroup
            name="gender"
            value={tempUser.gender}
            onChange={(e) => handleUserInput(e.target.name, e.target.value)}
          >
            <FormControlLabel value="male" control={<Radio />} label="Male" />
            <FormControlLabel
              value="female"
              control={<Radio />}
              label="Female"
            />
          </RadioGroup>
        </div>
        <div className={styles.input}>
          <label htmlFor='address'>Address</label>
          <TextField
            fullWidth
            required
            value={tempUser.address}
            id="address"
            placeholder="Enter Address"
            onChange={(e) => handleUserInput(e.target.id, e.target.value)}
          />
        </div>
      </div>
      <Button
        variant="contained"
        disabled={Object.values(tempUser).includes('')}
        onClick={() => addUser()}
      >
        Add User
      </Button>
    </>
  );
};

export default UserForm;
