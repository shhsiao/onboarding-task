import { fireEvent, render, screen } from '@testing-library/react';
import UserForm from '../UserForm';

describe('UserForm test', () => {
  const initUser = {
    first_name: '',
    last_name: '',
    age: 0,
    gender: 'male',
    address: '',
  };
  let handleUserInput = jest.fn(),
    addUser = jest.fn();
  it('should display header and related fields', () => {
    render(
      <UserForm
        handleUserInput={handleUserInput}
        addUser={addUser}
        tempUser={initUser}
      />
    );
    [
      'Users',
      'First Name',
      'Last Name',
      'Age',
      'Gender',
      'Male',
      'Female',
      'Address',
    ].forEach((v) => {
      const text = screen.getByText(v);
      expect(text).toBeInTheDocument();
    });
  });
  it('should display value based on tempUser', () => {
    render(
      <UserForm
        handleUserInput={handleUserInput}
        addUser={addUser}
        tempUser={{
          first_name: 'John',
          last_name: 'Doe',
          age: 20,
          gender: 'male',
          address: 'street',
        }}
      />
    );
    expect(screen.getByLabelText('First Name')).toHaveValue('John');
    expect(screen.getByLabelText('Last Name')).toHaveValue('Doe');
    expect(screen.getByLabelText('Age')).toHaveValue(20);
    expect(screen.getByLabelText('Address')).toHaveValue('street');
    expect(screen.getByLabelText('Male').hasAttribute('checked')).toBeTruthy();
  });
  it('btn should be disabled if tempUser has a empty field', () => {
    render(
      <UserForm
        handleUserInput={handleUserInput}
        addUser={addUser}
        tempUser={{
          first_name: 'John',
          last_name: '',
          age: 20,
          gender: 'male',
          address: 'street',
        }}
      />
    );
    expect(screen.getByText('Add User')).toHaveAttribute('disabled');
  });
  it('btn should be enabled if tempUser does not have empty fields', () => {
    render(
      <UserForm
        handleUserInput={handleUserInput}
        addUser={addUser}
        tempUser={{
          first_name: 'John',
          last_name: 'Doe',
          age: 20,
          gender: 'male',
          address: 'street',
        }}
      />
    );
    expect(screen.getByText('Add User')).not.toHaveAttribute('disabled');
  });
  it('should invode addUser function when btn is clicked', async () => {
    render(
      <UserForm
        handleUserInput={handleUserInput}
        addUser={addUser}
        tempUser={{
          first_name: 'John',
          last_name: 'Doe',
          age: 20,
          gender: 'male',
          address: 'street',
        }}
      />
    );
    const btn = screen.getByText('Add User');
    await fireEvent.click(btn);
    expect(addUser).toHaveBeenCalled();
  });
  it('should invoke handleUserInput when input is typed', async () => {
    render(
      <UserForm
        handleUserInput={handleUserInput}
        addUser={addUser}
        tempUser={initUser}
      />
    );
    let input = screen.getByLabelText('First Name');
    await fireEvent.change(input, { target: { value: 'John' } });
    expect(handleUserInput).toHaveBeenCalledWith('first_name', 'John');
     input = screen.getByLabelText('Last Name');
    await fireEvent.change(input, { target: { value: 'Doe' } });
    expect(handleUserInput).toHaveBeenCalledWith('last_name', 'Doe');
     input = screen.getByLabelText('Age');
    await fireEvent.change(input, { target: { value: 20 } });
    expect(handleUserInput).toHaveBeenCalledWith('age', '20');
     input = screen.getByLabelText('Address');
    await fireEvent.change(input, { target: { value: 'street' } });
    expect(handleUserInput).toHaveBeenCalledWith('address', 'street');
  });
});
